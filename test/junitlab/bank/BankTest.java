package junitlab.bank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import junitlab.bank.impl.FirstNationalBank;
import junitlab.bank.impl.GreatSavingsBank;

public class BankTest {

	Bank bank;

	@Before
	public void setUp() {
		bank = new GreatSavingsBank();
	}

	@Test
	public void testOpenAccount() throws Exception {
		String an = bank.openAccount();
		long balance = bank.getBalance(an);
		Assert.assertEquals("balance is not null", 0, balance);
	}

	@Test
	public void testUniqueAccount() {
		String an1 = bank.openAccount();
		String an2 = bank.openAccount();
		Assert.assertNotEquals("accountNumber is not unique", an1, an2);
	}

	@Test(expected = AccountNotExistsException.class)
	public void testInvalidAccount() throws Exception {
		bank.getBalance("almafa");
	}

	@Test
	public void testDeposit() throws Exception {
		String an = bank.openAccount();
		bank.deposit(an, 2000);
		long balance = bank.getBalance(an);
		Assert.assertEquals("deposit is not working properly", 2000, balance);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testNegativeDeposit() throws Exception {
		String an = bank.openAccount();
		bank.deposit(an, -1000);
	}
	
	@Test
	public void testCloseAccountWithZeroBalance() throws Exception {
		String an = bank.openAccount();
		Assert.assertTrue("closing account with zero balance", bank.closeAccount(an));
	}
	
	@Test
	public void testCloseAccountWithNonZeroBalance() throws Exception {
		String an = bank.openAccount();
		bank.deposit(an, 1000);
		Assert.assertFalse("closing account with non zero balance", bank.closeAccount(an));
	}
	
}
