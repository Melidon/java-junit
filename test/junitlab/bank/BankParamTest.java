package junitlab.bank;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import junitlab.bank.impl.GreatSavingsBank;

@RunWith(Parameterized.class)
public class BankParamTest {
	long amount;
	long rounded;
	Bank bank;

	public BankParamTest(long amount, long rounded) {
		this.amount = amount;
		this.rounded = rounded;
	}

	@Before
	public void setUp() {
		bank = new GreatSavingsBank();
	}
	
	@Test
	public void testWithdrawRounding() throws Exception {
		String an = bank.openAccount();
		bank.deposit(an, 10000);
		bank.withdraw(an, amount);
		Assert.assertEquals("rounding error", 10000-rounded, bank.getBalance(an));
	}

	@Parameters
	public static List<Object[]> parameters() {
		List<Object[]> params = new ArrayList<Object[]>();
		params.add(new Object[] {1100, 1100});
		params.add(new Object[] {1101, 1100});
		params.add(new Object[] {1149, 1100});
		params.add(new Object[] {1150, 1200});
		params.add(new Object[] {1151, 1200});
		params.add(new Object[] {1199, 1200});
		params.add(new Object[] {1200, 1200});
		return params;
	}
}
