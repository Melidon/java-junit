package junitlab.bank;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import junitlab.bank.impl.FirstNationalBank;
import junitlab.bank.impl.GreatSavingsBank;

public class BankTestFixture {

	Bank bank;
	String an1;
	String an2;

	@Before
	public void setUp() throws Exception {
		bank = new GreatSavingsBank();
		an1 = bank.openAccount();
		an2 = bank.openAccount();
		bank.deposit(an1, 1500);
		bank.deposit(an2, 12000);
	}

	@Test
	public void testTransfer() throws Exception {
		bank.transfer(an2, an1, 3456);
		long b1 = bank.getBalance(an1);
		long b2 = bank.getBalance(an2);
		Assert.assertEquals("targetAccount amount error", 4956, b1);
		Assert.assertEquals("sourceAccount amount error", 8544, b2);
	}

	@Test(expected = NotEnoughFundsException.class)
	public void testTransferWithoutEnoughFunds() throws Exception {
		bank.transfer(an1, an2, 3456);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalTransfer() throws Exception {
		bank.transfer(an1, an2, -1000);
	}
}
