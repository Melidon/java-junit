package junitlab.bank;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

import junitlab.bank.impl.GreatSavingsBank;

@RunWith(MockitoJUnitRunner.class)
public class NewBankTest {

	NewBank newBank;
	@Mock
	GreatSavingsBank mockBank;

	@Before
	public void setUp() {
		newBank = new NewBank(mockBank);
	}

	@Test
	public void testOpenAccountAndDeposit() throws Exception {
		String mystr = "almafa";
		long amount = 1000;
		when(mockBank.openAccount()).thenReturn(mystr);
		String an = newBank.openAccountAndDeposit(amount);
		Assert.assertEquals(mystr, an);
		verify(mockBank).openAccount();
		verify(mockBank).deposit(mystr, amount);
	}

	@Test
	public void testTransferAllAndCloseSourceAccount() throws AccountNotExistsException, NotEnoughFundsException {
		String mystr1 = "barackfa";
		String mystr2 = "k�rtefa";
		long amount = 2000;
		when(mockBank.closeAccount(mystr1)).thenReturn(true);
		when(mockBank.getBalance(mystr1)).thenReturn(amount);
		Assert.assertTrue("transferAllAndCloseSourceAccount failed", newBank.transferAllAndCloseSourceAccount(mystr1, mystr2));
		verify(mockBank).getBalance(mystr1);
		verify(mockBank).transfer(mystr1, mystr2, amount);
		verify(mockBank).closeAccount(mystr1);
	}

	@After
	public void checkForOtherInteractions() {
		verifyNoMoreInteractions(mockBank);
	}

}
